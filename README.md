# PruebaJAAS

Implementación de prueba del sistema nativo de seguridad de Java [JAAS](https://www.oracle.com/java/technologies/javase/javase-tech-security.html) (Java Authentication and Authorization Service)

Prueba realizada con `Java 11` y despliegue con `Tomcat 10`

### Credenciales de acceso

```
Usuario: user
Password: pass
```

### Licencia

MIT