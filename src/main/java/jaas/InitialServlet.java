package jaas;

import javax.security.auth.login.Configuration;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;

/**
 * Servlet implementation class InitialServlet
 */
public class InitialServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public InitialServlet() {
		super();
	}

	@Override
	public void init() throws ServletException {
		super.init();
		System.setProperty("java.security.auth.login.config", "classpath:jaas.conf");
		Configuration.getConfiguration().refresh();
	}
}
